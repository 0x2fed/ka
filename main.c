//	gcc main.c -o main
#include <stdio.h>
#include <stdint.h>

float dADC_to_PWM(float dADC) {//uint16_t uint16_t dADC
	float gamma;
	// 2048-2208.875
	if ((2048<dADC) && (dADC<=2208.875)) {
		return 0;
	}
	// 2208.875-2799.752
	// OUT: 0.200000-0.396526
	if ((2208.875<dADC) && (dADC<=2799.752)) {
		// gamma(dADC) = 3,326e-4*(dADC-2208,875)+0,2;
		gamma = 0.0003326*(dADC-2208.875)+0.2;
		return  127*(gamma-0.2)+1;
	}
	// 2799.752<dADC<4096
	// OUT: 0.396499-0.999566
	if ((2799.752<dADC) && (dADC<4096)) {
		// gamma(dADC) = 4,656e-4*(dADC-2377,719)+0,2; 
		gamma = 0.0004656*(dADC-2377.719)+0.2;
		return  127*(gamma-0.2)+1;
	}
	
	return 3;// ERROR
}






int main(void)
{
    //uint16_t pwm;
	float pwm;
	/*
	//	TEST 1.1 
	//	0; 2048<dADC<=2209.875
	pwm = dADC_to_PWM(2049);
    printf("pwm=%d\n", pwm);
	pwm = dADC_to_PWM(2208.875);
    printf("pwm=%d\n", pwm);	
	*/
	
	//	TEST 2.1 
	//	2208.875<dADC<=2799.752
	printf("TEST 2.1\n");
	pwm = dADC_to_PWM(2208.876);
    printf("pwm=%f\n", pwm);
	pwm = dADC_to_PWM(2799.752);
    printf("pwm=%f\n", pwm);		
	
	//	TEST 3.1 
	//	2799.752<dADC<4096
	printf("TEST 3.1\n");
	pwm = dADC_to_PWM(2799.753);
    printf("pwm=%f\n", pwm);
	pwm = dADC_to_PWM(4095);
    printf("pwm=%f\n", pwm);		
}